﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using sharifyapi.Entities;
using sharifyapi.Interfaces;
using sharifyapi.Services;
using System.Collections.Generic;
using System.Text.Json;
using sharifyapi;
using sharifyapi.DTO.Response;
using System.Linq;
using MockQueryable.NSubstitute;
using sharifyapi.DTO.Request;
using sharifyapi.Exceptions.ExceptionList;

namespace tests.Services
{
    [TestFixture]
    public class PlaylistServiceTests
    {
        private IRepository _repository;
        private IMapper _mapper;
        private UserManager<User> _userManager;
        private PlaylistService _playlistService;

        [SetUp]
        public void SetUp()
        {
            _repository = Substitute.For<IRepository>();
            var myProfile = new MappingProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
            _userManager = new UserManager<User>(Substitute.For<IUserStore<User>>(),
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);
            _playlistService = new PlaylistService(_repository, _mapper, _userManager);
        }

        private PlaylistService CreateService()
        {
            return new PlaylistService(
                _repository,
                _mapper,
                _userManager);
        }

        [Test]
        public async Task GetById_ReturnsPlaylitCorrectly()
        {
            // Arrange
            var service = CreateService();
            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var validUser = new User()
            {
                Id = userId,
                UserName = "validUser@gmail.com",
                Email = "validUser@gmail.com",
            };

            var validPlaylist = new Playlist()
            {
                Id = id,
                CreatedBy = validUser,
                LikedBy = new List<User>() { validUser },
            };

            var expected = _mapper.Map<PlaylistGeneralResponse>(validPlaylist);
            expected.IsLiked = true;
            expected.IsMine = true;
            expected.Genres = new List<string>();
            var expectedJson = JsonSerializer.Serialize(expected);

            _userManager.FindByIdAsync(userId.ToString()).Returns(validUser);
            _repository.GetByIdAsync<Playlist>(id).Returns(validPlaylist);

            // Act
            var result = await service.GetById(
                id,
                userId.ToString());

            var resultJson = JsonSerializer.Serialize(result);

            // Assert
            Assert.AreEqual(expectedJson, resultJson);
        }

        [Test]
        public async Task GetById_ReturnsPlaylist()
        {
            // Arrange
            var playlist = new Playlist()
            {
                Id = Guid.NewGuid(),
                Name = "Playlist",
                Description = "Description",
            };
            _repository.GetByIdAsync<Playlist>(default).ReturnsForAnyArgs(playlist);
            var expected = _mapper.Map<PlaylistGeneralResponse>(playlist);
            expected.Genres = new List<string>();
            var expectedJson = JsonSerializer.Serialize(expected);
            // Act
            var actual = await _playlistService.GetById(playlist.Id, string.Empty);
            var actualJson = JsonSerializer.Serialize(actual);
            // Assert
            Assert.AreEqual(expectedJson, actualJson);
        }

        [Test]
        public async Task GetMyPlaylists_ReturnsOnlyMyPlaylists()
        {
            // Arrange
            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "TestingUser",
            };

            var myPlaylist = new Playlist()
            {
                Id = Guid.NewGuid(),
                Name = "Playlist",
                CreatedBy = user,
            };

            var randomPlaylist = new Playlist()
            {
                Id = Guid.NewGuid(),
                Name = "Random",
                CreatedBy = new User(),
            };

            var playlists = new List<Playlist>() { myPlaylist, randomPlaylist }.AsQueryable().BuildMock();

            _repository.ListQuery<Playlist>().Returns(playlists);
            _userManager.FindByIdAsync(user.Id.ToString()).Returns(user);
            var expectedPlaylist = _mapper.Map<PlaylistGeneralResponse>(myPlaylist);
            expectedPlaylist.IsMine = true;
            expectedPlaylist.Genres = new List<string>();
            var expected = new PlaylistsGeneralResponse() { Playlists = new List<PlaylistGeneralResponse>() { expectedPlaylist } };
            var expectedJson = JsonSerializer.Serialize(expected);
            // Act
            var actual = await _playlistService.GetMyPlaylists(user.Id.ToString(), new FilterRequest());
            var actualJson = JsonSerializer.Serialize(actual);
            // Assert
            Assert.AreEqual(expectedJson, actualJson);
        }

        [Test]
        [TestCase("0519c4d2-4cdf-4060-a920-931a6e6b6636", "990571dc-0c0e-4822-a203-5cf582822e9b", "b503490a-fcf0-4424-853c-b77e2870540a")]
        public void AddSong_DoesNotThrowException_WhenValidDataIsPassed(Guid userId, Guid songId, Guid playlistId)
        {
            var validUserId = Guid.Parse("0519c4d2-4cdf-4060-a920-931a6e6b6636");
            var validSongId = Guid.Parse("990571dc-0c0e-4822-a203-5cf582822e9b");
            var validPlaylistId = Guid.Parse("b503490a-fcf0-4424-853c-b77e2870540a");
            // Arrange
            var validUser = new User()
            {
                Id = validUserId,
                UserName = "TestingUser",
            };

            var validSong = new Song()
            {
                Id = validSongId,
                Genre = Genre.Classical,
                Status = Status.Confirmed
            };

            var validPlaylist = new Playlist()
            {
                Id = validPlaylistId,
                Name = "Playlist",
                CreatedBy = validUser,
            };

            _repository.GetByIdAsync<Song>(validSongId).Returns(validSong);
            _repository.GetByIdAsync<Playlist>(validPlaylistId).Returns(validPlaylist);
            _userManager.FindByIdAsync(validUser.Id.ToString()).Returns(validUser);
            // Act
            var request = new SongOfPlaylistRequest()
            {
                PlaylistId = playlistId,
                SongId = songId,
            };
            // Assert
            Assert.DoesNotThrowAsync(async () => await _playlistService.AddSong(request, userId.ToString()));
        }

        [Test]
        // Data does not exist by id
        [TestCase("00000000-0000-0000-0000-000000000000", "990571dc-0c0e-4822-a203-5cf582822e9b", "b503490a-fcf0-4424-853c-b77e2870540a")]
        [TestCase("0519c4d2-4cdf-4060-a920-931a6e6b6636", null, "b503490a-fcf0-4424-853c-b77e2870540a")]
        [TestCase("0519c4d2-4cdf-4060-a920-931a6e6b6636", "990571dc-0c0e-4822-a203-5cf582822e9b", null)]
        // Playlist not created by user
        [TestCase("0519c4d2-4cdf-4060-a920-931a6e6b6636", "990571dc-0c0e-4822-a203-5cf582822e9b", "cbb44c04-bd06-4412-b4e4-9a21eeec15eb")]
        public void AddSong_ThrowsException_WhenInvalidDataIsPassed(Guid userId, Guid songId, Guid playlistId)
        {
            var validUserId = Guid.Parse("0519c4d2-4cdf-4060-a920-931a6e6b6636");
            var validSongId = Guid.Parse("990571dc-0c0e-4822-a203-5cf582822e9b");
            var validPlaylistId = Guid.Parse("b503490a-fcf0-4424-853c-b77e2870540a");
            var invalidPlaylistCreatedBySomeoneElseId = Guid.Parse("cbb44c04-bd06-4412-b4e4-9a21eeec15eb");
            // Arrange
            var validUser = new User()
            {
                Id = validUserId,
                UserName = "TestingUser",
            };

            var validSong = new Song()
            {
                Id = validSongId,
                Genre = Genre.Classical,
                Status = Status.Confirmed,
            };

            var validPlaylist = new Playlist()
            {
                Id = validPlaylistId,
                Name = "Playlist",
                CreatedBy = validUser,
            };

            var invalidPlaylistCreatedBySomeoneElse = new Playlist()
            {
                Id = validPlaylistId,
                Name = "Playlist",
                CreatedBy = new User(),
            };

            _repository.GetByIdAsync<Song>(validSongId).Returns(validSong);
            _repository.GetByIdAsync<Playlist>(validPlaylistId).Returns(validPlaylist);
            _repository.GetByIdAsync<Playlist>(invalidPlaylistCreatedBySomeoneElseId).Returns(invalidPlaylistCreatedBySomeoneElse);
            _userManager.FindByIdAsync(validUser.Id.ToString()).Returns(validUser);
            // Act
            var request = new SongOfPlaylistRequest()
            {
                PlaylistId = playlistId,
                SongId = songId,
            };
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _playlistService.AddSong(request, userId.ToString()));
        }
    }
}
