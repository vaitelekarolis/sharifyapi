using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using NSubstitute;
using NUnit.Framework;
using sharifyapi;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;
using sharifyapi.Exceptions.ExceptionList;
using sharifyapi.Interfaces;
using sharifyapi.Services;
using System.Collections.Generic;
using System.Text.Json;

namespace tests.Services
{
    public class UserServiceTests
    {
        private IRepository _repository;
        private IUserService _userService;
        private ITokenClaimsService _tokenClaimsService;
        private ISongService _songService;
        private IMapper _mapper;
        private IUserStore<User> _userStore;
        private UserManager<User> userManager;
        private SignInManager<User> signInManager;

        [SetUp]
        public void Setup()
        {
            _repository = Substitute.For<IRepository>();
            _userStore = Substitute.For<IUserStore<User>>();
            _tokenClaimsService = Substitute.For<ITokenClaimsService>();
            _songService = Substitute.For<ISongService>();
            userManager = new UserManager<User>(_userStore,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null); ;
            signInManager = new SignInManager<User>(userManager,
                                                    Substitute.For<IHttpContextAccessor>(),
                                                    Substitute.For<IUserClaimsPrincipalFactory<User>>(),
                                                    null,
                                                    null,
                                                    null,
                                                    null);
            var myProfile = new MappingProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
            _userService = new UserService(userManager, signInManager, _tokenClaimsService, _mapper, _repository, _songService);
        }

        [Test]
        public void Authenticate_ThrowsException_WhenUserDoesNotExist()
        {
            // Arrange
            var authenticateRequest = new AuthenticateRequest() { Email = "karolisvaitele@gmail.com", Password = "Blabla" };
            // Act
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _userService.Authenticate(authenticateRequest));
        }

        [Test]
        public void GetGenreOfUser_Returns_CorrectGenre()
        {
            // Arrange
            var expectedGenre = Genre.Rap;
            var userSong = new Song
            {
                Genre = expectedGenre,
                Title = "Test",
                Status = Status.Confirmed,
            };
            var user = new User
            {
                AddedSongs = new List<Song> { userSong },
            };
            var expected = new List<string> { expectedGenre.ToString() };
            var expectedJson = JsonSerializer.Serialize(expected);
            //Act
            var actual = _userService.GetGenreOfUser(user);
            var actualJson = JsonSerializer.Serialize(actual);
            //Assert
            Assert.AreEqual(expectedJson, actualJson);
        }

        [Test]
        public void GetGenreOfUser_Returns_CorrectGenres()
        {
            // Arrange
            var expectedGenre1 = Genre.Rap;
            var expectedGenre2 = Genre.EDM;
            var userSong1 = new Song
            {
                Genre = expectedGenre1,
                Title = "Test",
                Status = Status.Confirmed,
            };
            var userSong2 = new Song
            {
                Genre = expectedGenre2,
                Title = "Test",
                Status = Status.Confirmed,
            };
            var user = new User
            {
                AddedSongs = new List<Song> { userSong1, userSong2 },
            };
            var expected = new List<string> { expectedGenre1.ToString(), expectedGenre2.ToString() };
            var expectedJson = JsonSerializer.Serialize(expected);
            //Act
            var actual = _userService.GetGenreOfUser(user);
            var actualJson = JsonSerializer.Serialize(actual);
            //Assert
            Assert.AreEqual(expectedJson, actualJson);
        }
    }
}