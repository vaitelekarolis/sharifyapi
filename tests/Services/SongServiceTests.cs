﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using MockQueryable.NSubstitute;
using NSubstitute;
using NUnit.Framework;
using sharifyapi;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Entities;
using sharifyapi.Exceptions.ExceptionList;
using sharifyapi.Interfaces;
using sharifyapi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace tests.Services
{
    public class SongServiceTests
    {
        private IRepository _repository;
        private ISongService _songService;
        private IMapper _mapper;
        private IUserStore<User> _userStore;
        private UserManager<User> _userManager;

        [SetUp]
        public void Setup()
        {
            _repository = Substitute.For<IRepository>();
            _userStore = Substitute.For<IUserStore<User>>();
            var storageService = Substitute.For<IStorageService>();
            _userManager = new UserManager<User>(_userStore,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);
            var myProfile = new MappingProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);

            _songService = new SongService(_repository, _mapper, _userManager, storageService);
        }

        [Test]
        public async Task GetSongRequests_ReturnsAllSongs()
        {
            // Arrange
            var songRequest = new Song()
            {
                Status = Status.Pending,
                Title = "SongRequest"
            };
            var confirmedSong = new Song()
            {
                Status = Status.Confirmed,
                Title = "ConfirmedSong"
            };
            var expected = new MySongsResponse() { Songs = new List<MySongResponse>() { _mapper.Map<MySongResponse>(songRequest), _mapper.Map<MySongResponse>(confirmedSong) } };
            var expectedJson = JsonSerializer.Serialize(expected);

            var queryableList = new List<Song>() { songRequest, confirmedSong }.AsQueryable().BuildMock();
            _repository.ListQuery<Song>().Returns(queryableList);
            // Act
            var actual = await _songService.GetSongRequests(new FilterRequest());
            var actualJson = JsonSerializer.Serialize(actual);
            // Assert
            Assert.AreEqual(expectedJson, actualJson);
        }

        [Test]
        public void ConfirmSong_ThrowsExceptionIfSongIsRejected()
        {
            // Arrange
            var rejectedSong = new Song()
            {
                Id = Guid.NewGuid(),
                Status = Status.Rejected,
                Title = "RejectedSong"
            };
            // Act
            var idRequest = new IdRequest() { Id = Guid.NewGuid() };
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _songService.ConfirmSong(idRequest));
        }

        [Test]
        public void ConfirmSong_ThrowsExceptionIfSongDoesNotExist()
        {
            // Arrange
            // Act
            var idRequest = new IdRequest() { Id = Guid.NewGuid() };
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _songService.ConfirmSong(idRequest));
        }

        [Test]
        public void RejectSong_ThrowsExceptionIfSongIsConfirmed()
        {
            // Arrange
            var confirmedSong = new Song()
            {
                Id = Guid.NewGuid(),
                Status = Status.Confirmed,
                Title = "ConfirmedSong",
            };
            // Act
            var idRequest = new IdRequest() { Id = Guid.NewGuid() };
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _songService.RejectSong(idRequest));
        }

        [Test]
        public void RejectSong_ThrowsExceptionIfSongDoesNotExist()
        {
            // Arrange
            // Act
            var idRequest = new IdRequest() { Id = Guid.NewGuid() };
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _songService.ConfirmSong(idRequest));
        }

        [Test]
        public async Task GetAll_ReturnsAll()
        {
            // Arrange
            var song1 = new Song
            {
                Status = Status.Confirmed,
            };
            var song2 = new Song
            {
                Status = Status.Confirmed,
            };
            var filter = new FilterRequest();

            var queryable = new List<Song> { song1, song2 }.AsQueryable().BuildMock();
            _repository.ListQuery<Song>().Returns(queryable);
            var expectedSongResponse1 = _mapper.Map<SongResponse>(song1);
            var expectedSongResponse2 = _mapper.Map<SongResponse>(song2);
            var expected = new SongsResponse { Songs = new List<SongResponse> { expectedSongResponse1, expectedSongResponse2 } };
            var expectedJson = JsonSerializer.Serialize(expected);

            // Act
            var actual = await _songService.GetAll("random", filter);
            var actualJson = JsonSerializer.Serialize(actual);

            // Assert
            Assert.AreEqual(expectedJson, actualJson);
        }

        [Test]
        public async Task GetPlaySong_ReturnsOnlySongName_WhenPlayingNotPlaylist()
        {
            // Arrange
            var songId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var expectedSongName = "TestSong";

            var user = new User
            {
                Id = userId,
            };

            var song1 = new Song
            {
                SongName = expectedSongName,
                Id = songId,
            };

            var song2 = new Song
            {
                SongName = "AnotherSong",
                Id = Guid.NewGuid(),
            };

            var playlist = new Playlist
            {
                Name = "Test",
                Songs = new List<Song> { song1, song2 }
            };

            _repository.GetByIdAsync<Song>(songId).Returns(song1);
            _repository.GetByIdAsync<Playlist>(Arg.Any<Guid>()).Returns(playlist);
            _userManager.FindByIdAsync(userId.ToString()).Returns(user);
            var expectedResponse = new PlaySongResponse 
            {
                Id = songId,
                Genre = Genre.Classical.ToString(),
                SongName = expectedSongName,
            };

            var expectedResponseJson = JsonSerializer.Serialize(expectedResponse);

            //Act
            var playSongRequest = new PlaySongRequest
            {
                SongId = songId,
            };

            var playSongResponse = await _songService.GetPlaySong(userId.ToString(), playSongRequest);
            var actualJson = JsonSerializer.Serialize(playSongResponse);
            //Assert
            Assert.AreEqual(expectedResponseJson, actualJson);
        }

        [Test]
        public async Task GetPlaySong_ReturnsFullResponse_WhenPlayingPlaylist()
        {
            // Arrange
            var previousSongId = Guid.NewGuid();
            var songId = Guid.NewGuid();
            var nextSongId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var expectedSongName = "TestSong";
            var playlistId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
            };

            var song0 = new Song
            {
                Id = previousSongId,
            };

            var song1 = new Song
            {
                SongName = expectedSongName,
                Id = songId,
            };

            var song2 = new Song
            {
                Id = nextSongId,
            };

            var playlist = new Playlist
            {
                Id = playlistId,
                Name = "Test",
                Songs = new List<Song> { song0, song1, song2 }
            };

            _repository.GetByIdAsync<Song>(songId).Returns(song1);
            _repository.GetByIdAsync<Playlist>(playlistId).Returns(playlist);
            _userManager.FindByIdAsync(userId.ToString()).Returns(user);

            var expectedResponse = new PlaySongResponse
            {
                Id = songId,
                Genre = Genre.Classical.ToString(),
                SongName = expectedSongName,
                PreviousSongId = previousSongId,
                NextSongId = nextSongId,
            };

            var expectedResponseJson = JsonSerializer.Serialize(expectedResponse);

            //Act
            var playSongRequest = new PlaySongRequest
            {
                SongId = songId,
                PlaylistId = playlistId,
            };

            var playSongResponse = await _songService.GetPlaySong(userId.ToString(), playSongRequest);
            var actualJson = JsonSerializer.Serialize(playSongResponse);
            //Assert
            Assert.AreEqual(expectedResponseJson, actualJson);
        }
    }
}
