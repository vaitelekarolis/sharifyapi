﻿using Microsoft.AspNetCore.Identity;
using sharifyapi.Entities;

namespace tests
{
    public interface IUserStore<T> : IUserEmailStore<User>,
        IUserLoginStore<User>,
        IUserPasswordStore<User>
    {
    }
}
