﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace sharifyapi.Migrations
{
    public partial class ChangeSongStatusLogic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsConfirmed",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "IsRejected",
                table: "Songs");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Songs",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Songs");

            migrationBuilder.AddColumn<bool>(
                name: "IsConfirmed",
                table: "Songs",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsRejected",
                table: "Songs",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
