﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace sharifyapi.Migrations
{
    public partial class AddProperLikedPlaylists : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Playlists_Users_CreatedById",
                table: "Playlists");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Playlists_PlaylistId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_PlaylistId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PlaylistId",
                table: "Users");

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedById",
                table: "Playlists",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.CreateTable(
                name: "PlaylistUser",
                columns: table => new
                {
                    LikedById = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LikedPlaylistsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlaylistUser", x => new { x.LikedById, x.LikedPlaylistsId });
                    table.ForeignKey(
                        name: "FK_PlaylistUser_Playlists_LikedPlaylistsId",
                        column: x => x.LikedPlaylistsId,
                        principalTable: "Playlists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlaylistUser_Users_LikedById",
                        column: x => x.LikedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlaylistUser_LikedPlaylistsId",
                table: "PlaylistUser",
                column: "LikedPlaylistsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Playlists_Users_CreatedById",
                table: "Playlists",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Playlists_Users_CreatedById",
                table: "Playlists");

            migrationBuilder.DropTable(
                name: "PlaylistUser");

            migrationBuilder.AddColumn<Guid>(
                name: "PlaylistId",
                table: "Users",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedById",
                table: "Playlists",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_PlaylistId",
                table: "Users",
                column: "PlaylistId");

            migrationBuilder.AddForeignKey(
                name: "FK_Playlists_Users_CreatedById",
                table: "Playlists",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Playlists_PlaylistId",
                table: "Users",
                column: "PlaylistId",
                principalTable: "Playlists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
