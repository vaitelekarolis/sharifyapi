﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace sharifyapi.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDisplay(this Enum enumValue)
        {
            try
            {
                return enumValue.GetType().GetMember(enumValue.ToString())
                .FirstOrDefault()?
                .GetCustomAttribute<DisplayAttribute>()?
                .GetName() ?? "Unknown";
            }
            catch
            {
                return null;
            }
        }
    }
}
