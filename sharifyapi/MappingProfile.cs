﻿using System;
using System.Linq;
using AutoMapper;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Entities;

namespace sharifyapi
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            MapUsers();
            MapSongs();
            MapPlaylists();
        }

        private void MapUsers()
        {
            CreateMap<AuthenticateRequest, User>().ReverseMap();
            CreateMap<RegisterRequest, User>().ReverseMap();
            CreateMap<UserResponse, User>().ReverseMap();
            CreateMap<User, UserInfoResponse>().ReverseMap();
        }

        private void MapSongs()
        {
            CreateMap<Song, SongResponse>()
                .ForMember(dto => dto.Author, opt => opt.MapFrom(src => src.Author.Name))
                .ForMember(dto => dto.Genre, opt => opt.MapFrom(src => src.Genre.ToString()))
                .ForMember(dto => dto.Picture, opt => opt.MapFrom(src => src.Author.Picture))
                .IncludeAllDerived()
                .ReverseMap();

            CreateMap<Song, MySongResponse>()
                .ForMember(dto => dto.Status, opt => opt.MapFrom(src => src.Status.ToString()))
                .ReverseMap();

            CreateMap<CreateSongRequest, Song>()
                .ForMember(dto => dto.Genre, opt => opt.MapFrom(src => (Genre)Enum.Parse(typeof(Genre), src.Genre)))
                .ReverseMap();

            CreateMap<Song, PlaySongResponse>()
                .ReverseMap();
        }

        private void MapPlaylists()
        {
            CreateMap<CreatePlaylistRequest, Playlist>().ReverseMap();

            CreateMap<Playlist, PlaylistGeneralResponse>()
                .ForMember(dto => dto.LikeCount, opt => opt.MapFrom(src => src.LikedBy.Count))
                .ForMember(dto => dto.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy.Name))
                .ForMember(dto => dto.CreatedByPicture, opt => opt.MapFrom(src => src.CreatedBy.Picture))
                .ForMember(dto => dto.FirstSongId, opt => opt.MapFrom(src => src.Songs.Select(x => (Guid?)x.Id).FirstOrDefault()))
                .IncludeAllDerived()
                .ReverseMap();

            CreateMap<Playlist, SongPlaylistsResponse>()
                .ReverseMap();
        }
    }
}
