﻿namespace sharifyapi
{
    public static class Constants
    {
        public const string AUTH_KEY = "AuthKeyOfDoomThatMustBeAMinimumNumberOfBytesAndIsSuperSecure";
        public const string JWT_SECRET_KEY = "AuthKeyOfDoomThatMustBeAMinimumNumberOfBytesAndIsSuperSecure";

        public const string JWT_TOKEN_COOKIE_NAME = "Auth_token";

        public static class Roles
        {
            public const string ADMIN = "Admin";
            public const string USER = "User";
        }
    }
}
