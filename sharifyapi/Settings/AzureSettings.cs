﻿namespace sharifyapi.Settings
{
    public class AzureSettings
    {
        public string BlobConnectionString { get; set; }

        public string SongsContainer { get; set; }
    }
}
