﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sharifyapi.Entities
{
    public enum Genre
    {
        [Display(Name = "Classical")]
        Classical,
        [Display(Name = "Country")]
        Country,
        [Display(Name = "Electronic dance music")]
        EDM,
        [Display(Name = "Hip-Hop")]
        HipHop,
        [Display(Name = "Indie Rock")]
        IndieRock,
        [Display(Name = "Jazz")]
        Jazz,
        [Display(Name = "K-pop")]
        Kpop,
        [Display(Name = "Metal")]
        Metal,
        [Display(Name = "Oldies")]
        Oldies,
        [Display(Name = "Pop")]
        Pop,
        [Display(Name = "Rap")]
        Rap,
        [Display(Name = "Rhythm & blues")]
        RB,
        [Display(Name = "Rock")]
        Rock,
        [Display(Name = "Techno")]
        Techno,
    }

    public enum Status
    {
        Pending,
        Confirmed,
        Rejected,
    }

    public class Song : BaseEntity
    {
        public Song()
        {
            Playlists = new List<Playlist>();
            LikedBy = new List<User>();
        }

        public string Title { get; set; }

        public Genre Genre { get; set; }

        public string SongName { get; set; }

        public Status Status { get; set; }

        public long ListenCount { get; set; }

        public Guid? AuthorId { get; set; }

        public virtual User Author { get; set; }

        public virtual ICollection<Playlist> Playlists { get; set; }

        public virtual ICollection<User> LikedBy { get; set; }
    }
}
