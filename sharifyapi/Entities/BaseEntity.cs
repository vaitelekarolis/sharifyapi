﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace sharifyapi.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTimeOffset CreationDate { get; set; }
    }
}
