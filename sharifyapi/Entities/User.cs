﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace sharifyapi.Entities
{
    public class User : IdentityUser<Guid>
    {
        public User()
        {
            Playlists = new List<Playlist>();
            LikedPlaylists = new List<Playlist>();
            AddedSongs = new List<Song>();
            LikedSongs = new List<Song>();
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Picture { get; set; }

        public virtual IList<Playlist> Playlists { get; set; }

        public virtual IList<Playlist> LikedPlaylists { get; set; }

        public virtual IList<Song> AddedSongs { get; set; }

        public virtual IList<Song> LikedSongs { get; set; }
    }
}
