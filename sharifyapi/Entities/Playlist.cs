﻿using System;
using System.Collections.Generic;

namespace sharifyapi.Entities
{
    public class Playlist : BaseEntity
    {
        public Playlist()
        {
            Songs = new List<Song>();
            LikedBy = new List<User>();
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsShared { get; set; } = false;

        public Guid? CreatedById { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual IList<Song> Songs { get; set; }

        public virtual IList<User> LikedBy { get; set; }
    }
}
