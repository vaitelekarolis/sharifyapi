﻿using System;
using System.Threading.Tasks;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;

namespace sharifyapi.Interfaces
{
    public interface IPlaylistService
    {
        public Task CreatePlaylist(CreatePlaylistRequest request, string id);

        public Task AddSong(SongOfPlaylistRequest request, string id);

        public Task RemoveSong(SongOfPlaylistRequest request, string id);

        public Task UpdatePlaylist(UpdatePlaylistRequest request, string id);

        public Task DeletePlaylist(IdRequest request, string id);

        public Task ToggleSharePaylist(IdRequest request, string id);

        public Task ToggleLikePlaylist(IdRequest request, string id);

        public Task<PlaylistGeneralResponse> GetById(Guid id, string userId);

        public Task<PlaylistsGeneralResponse> GetMyPlaylists(string id, FilterRequest filter);

        public Task<PlaylistsGeneralResponse> GetLikedPlaylists(string id, FilterRequest filter);

        public Task<SongsPlaylistsResponse> GetSongsPlaylists(Guid songId, string userId);

        public Task<PlaylistsGeneralResponse> GetAllShared(string id, FilterRequest filter);

        public Task<SongsResponse> GetSongs(Guid id, FilterRequest filter);
    }
}
