﻿using System.Collections.Generic;
using System.Threading.Tasks;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Entities;

namespace sharifyapi.Interfaces
{
    public interface IUserService
    {
        public Task<AuthenticateResponse> Authenticate(AuthenticateRequest request);

        public Task<UserResponse> Register(RegisterRequest request);

        public Task Logout();

        public Task<UserResponse> GetUser(string id);

        public Task<UserInfoResponse> GetUserByName(string username);

        public Task<UsersResponse> GetAll();

        public Task Delete(string userId);

        Task UpdateProfilePicture(string userId, string profilePicture);

        Task UpdateDescription(string userId, string description);

        List<string> GetGenreOfUser(User user);
    }
}
