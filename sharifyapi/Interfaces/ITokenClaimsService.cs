﻿using System.Threading.Tasks;

namespace sharifyapi.Interfaces
{
    public interface ITokenClaimsService
    {
        public Task<string> GetTokenAsync(string userName);
    }
}
