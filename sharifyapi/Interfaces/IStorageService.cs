﻿using System.Threading.Tasks;

namespace sharifyapi.Interfaces
{
    public interface IStorageService
    {
        Task UploadSongAsync(string fileInBase64, string fileType, string fileName);

        Task DeleteSongAsync(string fileName);
    }
}
