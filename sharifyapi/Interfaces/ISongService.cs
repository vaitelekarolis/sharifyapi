﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Entities;

namespace sharifyapi.Interfaces
{
    public interface ISongService
    {
        public Task<SongsResponse> GetAll(string userId, FilterRequest filter);

        public Task<SongsResponse> GetTopPlayed(string userId);

        public Task<SongsResponse> GetNewest(string userId);

        public Task<SongResponse> GetById(Guid id, string userId);

        public Task<PlaySongResponse> GetPlaySong(string userId, PlaySongRequest request);

        public Task<SongsResponse> GetLiked(string userId, FilterRequest filter);

        public Task<SongsResponse> GetCreatedBy(string userName, FilterRequest filter, int? takeTop);

        public Task<MySongsResponse> GetMy(string userId, FilterRequest filter);

        public Task<MySongsResponse> GetSongRequests(FilterRequest filter);

        public List<string> GetSongGenres();

        public List<string> GetSongStatuses();

        public Task ToggleLikeSong(IdRequest request, string id);

        public Task CreateSong(CreateSongRequest request, string userId);

        public Task ConfirmSong(IdRequest request);

        public Task RejectSong(IdRequest request);

        public Task DeleteSong(string userId, Guid songId);

        public Task DeleteSongs(User user);
    }
}
