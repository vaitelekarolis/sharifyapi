using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using sharifyapi.Entities;
using sharifyapi.Infrastructure;
using sharifyapi.Interfaces;

namespace sharifyapi
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<SharifyDbContext>();

                var userManager = services.GetRequiredService<UserManager<User>>();
                var roleManager = services.GetRequiredService<RoleManager<Role>>();
                var songService = services.GetRequiredService<ISongService>();
                await SharifyDbContextSeed.SeedAuthAsync(userManager, roleManager);

                // SEED SONGS
                //var users = await SharifyDbContextSeed.SeedUsersAsync(userManager, roleManager);
                //await SharifyDbContextSeed.SeedDataAsync(songService, users);
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
