﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Playlists
{
    public class PlaylistFilter : IPlaylistFilter
    {
        private IPlaylistFilter _nextFilter;

        public IPlaylistFilter SetNext(IPlaylistFilter filter)
        {
            _nextFilter = filter;
            return filter;
        }

        public virtual IQueryable<Playlist> Filter(IQueryable<Playlist> playlists, FilterRequest filterRequest)
        {
            if (_nextFilter != null)
            {
                return _nextFilter.Filter(playlists, filterRequest);
            }

            return playlists;
        }
    }
}
