﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Playlists
{
    public interface IPlaylistFilter
    {
        IPlaylistFilter SetNext(IPlaylistFilter filter);

        IQueryable<Playlist> Filter(IQueryable<Playlist> playlists, FilterRequest filterRequest);
    }
}
