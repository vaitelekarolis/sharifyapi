﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Playlists
{
    public class PlaylistSearchFilter : PlaylistFilter
    {
        public override IQueryable<Playlist> Filter(
            IQueryable<Playlist> playlists,
            FilterRequest filterRequest)
        {
            var search = filterRequest.Search?.Trim();
            var filteredPlaylists = playlists;
            if (!string.IsNullOrEmpty(search))
            {
                filteredPlaylists = playlists.Where(x => x.Name.Contains(search)
                    || x.Description.Contains(search));
            }

            return base.Filter(filteredPlaylists, filterRequest);
        }
    }
}
