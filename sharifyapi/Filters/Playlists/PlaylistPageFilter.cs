﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Playlists
{
    public class PlaylistPageFilter : PlaylistFilter
    {
        public override IQueryable<Playlist> Filter(
            IQueryable<Playlist> playlists,
            FilterRequest filterRequest)
        {
            var filteredPlaylists = playlists;

            if (filterRequest.Page.HasValue)
            {
                filteredPlaylists = filteredPlaylists.Skip((filterRequest.Page.Value - 1) * filterRequest.PageSize)
                .Take(filterRequest.PageSize);
            }

            return base.Filter(filteredPlaylists, filterRequest);
        }
    }
}
