﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Songs
{
    public abstract class SongFilter : ISongFilter
    {
        private ISongFilter _nextFilter;

        public ISongFilter SetNext(ISongFilter filter)
        {
            _nextFilter = filter;
            return filter;
        }

        public virtual IQueryable<Song> Filter(IQueryable<Song> songs, FilterRequest filterRequest)
        {
            if (_nextFilter != null)
            {
                return _nextFilter.Filter(songs, filterRequest);
            }

            return songs;
        }
    }
}
