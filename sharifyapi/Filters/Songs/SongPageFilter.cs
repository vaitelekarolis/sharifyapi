﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Songs
{
    public class SongPageFilter : SongFilter
    {
        public override IQueryable<Song> Filter(
            IQueryable<Song> songs,
            FilterRequest filterRequest)
        {
            var filteredSongs = songs;

            if (filterRequest.Page.HasValue)
            {
                filteredSongs = filteredSongs.Skip((filterRequest.Page.Value - 1) * filterRequest.PageSize)
                .Take(filterRequest.PageSize);
            }

            return base.Filter(filteredSongs, filterRequest);
        }
    }
}
