﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Songs
{
    public class SongSearchFilter : SongFilter
    {
        public override IQueryable<Song> Filter(
            IQueryable<Song> songs,
            FilterRequest filterRequest)
        {
            var search = filterRequest.Search?.Trim();
            var filteredSongs = songs;
            if (!string.IsNullOrEmpty(search))
            {
                filteredSongs = songs.Where(x => x.Title.Contains(search)
                    || x.Author.Name.Contains(search));
            }

            return base.Filter(filteredSongs, filterRequest);
        }
    }
}
