﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Songs
{
    public class SongGenreFilter : SongFilter
    {
        public override IQueryable<Song> Filter(
            IQueryable<Song> songs,
            FilterRequest filterRequest)
        {
            var genres = filterRequest.GetGenresList();
            var filteredSongs = songs;
            if (genres.Count != 0)
            {
                filteredSongs = songs.Where(x => genres.Contains(x.Genre));
            }

            return base.Filter(filteredSongs, filterRequest);
        }
    }
}
