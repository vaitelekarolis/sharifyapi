﻿using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Songs
{
    public interface ISongFilter
    {
        ISongFilter SetNext(ISongFilter filter);

        IQueryable<Song> Filter(IQueryable<Song> songs, FilterRequest filterRequest);
    }
}
