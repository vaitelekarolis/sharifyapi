﻿using System;
using System.Linq;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;

namespace sharifyapi.Filters.Songs
{
    public class SongStatusFilter : SongFilter
    {
        public override IQueryable<Song> Filter(
            IQueryable<Song> songs,
            FilterRequest filterRequest)
        {
            var status = filterRequest.Status;
            var filteredSongs = songs;
            if (!string.IsNullOrEmpty(status))
            {
                var statusEnum = (Status)Enum.Parse(typeof(Status), status);
                filteredSongs = songs.Where(x => statusEnum == x.Status);
            }

            return base.Filter(filteredSongs, filterRequest);
        }
    }
}
