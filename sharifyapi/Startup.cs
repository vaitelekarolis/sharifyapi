using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using sharifyapi.Entities;
using sharifyapi.Exceptions;
using sharifyapi.Infrastructure;
using sharifyapi.Interfaces;
using sharifyapi.Services;
using sharifyapi.Settings;

namespace sharifyapi
{
    public class Startup
    {
        private const string AZURE_SETTINGS_NAME = "Azure";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            AddControllers(services);

            AddServices(services);

            services.AddDbContext<SharifyDbContext>(
                options =>
                options.UseLazyLoadingProxies()
                    .UseSqlServer(Configuration.GetConnectionString("AppConnection")), ServiceLifetime.Scoped);

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<SharifyDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 8;
            });

            services.AddHttpClient();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "sharifyapi", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer",
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new string[] { }
                    },
                });
            });

            var key = Encoding.ASCII.GetBytes(Constants.AUTH_KEY);

            services.AddAuthentication(config =>
            {
                config.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                config.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(config =>
                {
                    config.RequireHttpsMetadata = false;
                    config.SaveToken = true;
                    config.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                    };
                    config.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            context.Token = context.Request.Cookies[Constants.JWT_TOKEN_COOKIE_NAME];
                            return Task.CompletedTask;
                        },
                    };
                });
            services.AddAuthorization();

            services.AddAutoMapper(typeof(Startup).Assembly);
            ConfigureAppSettings(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Swagger for prod too
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "sharifyapi v1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(options =>
                    options.SetIsOriginAllowed(_ => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public static void AddServices(IServiceCollection services)
        {
            services.AddScoped<IRepository, Repository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITokenClaimsService, TokenClaimsService>();
            services.AddScoped<ISongService, SongService>();
            services.AddScoped<IPlaylistService, PlaylistService>();
            services.AddScoped<IStorageService, StorageService>();
        }

        private static void AddControllers(IServiceCollection services)
        {
            services.AddControllers();
            services.AddControllers(options =>
                options.Filters.Add<ExceptionFilter>());
        }

        private void ConfigureAppSettings(IServiceCollection services)
        {
            services.Configure<AzureSettings>(Configuration.GetSection(AZURE_SETTINGS_NAME))
                .AddSingleton(sp => sp.GetRequiredService<IOptions<AzureSettings>>().Value);
        }
    }
}
