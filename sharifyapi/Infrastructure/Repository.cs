﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using sharifyapi.Entities;
using sharifyapi.Interfaces;

namespace sharifyapi.Infrastructure
{
    public class Repository : IRepository
    {
        private readonly SharifyDbContext _dbContext;

        public Repository(SharifyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public T GetById<T>(Guid id)
            where T : BaseEntity
        {
            return _dbContext.Set<T>().SingleOrDefault(e => e.Id == id);
        }

        public Task<T> GetByIdAsync<T>(Guid id)
            where T : BaseEntity
        {
            return _dbContext.Set<T>().SingleOrDefaultAsync(e => e.Id == id);
        }

        public Task<List<T>> ListAsync<T>()
            where T : BaseEntity
        {
            return _dbContext.Set<T>().ToListAsync();
        }

        public IQueryable<T> ListQuery<T>()
            where T : BaseEntity
        {
            return _dbContext.Set<T>();
        }

        public async Task<T> AddAsync<T>(T entity)
            where T : BaseEntity
        {
            await _dbContext.Set<T>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public Task UpdateAsync<T>(T entity)
            where T : BaseEntity
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteAsync<T>(T entity)
            where T : BaseEntity
        {
            _dbContext.Set<T>().Remove(entity);
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteRangeAsync<T>(IEnumerable<T> entity)
            where T : BaseEntity
        {
            _dbContext.Set<T>().RemoveRange(entity);
            return _dbContext.SaveChangesAsync();
        }
    }
}
