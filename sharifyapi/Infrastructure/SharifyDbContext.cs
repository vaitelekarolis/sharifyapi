﻿using System;
using System.Reflection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using sharifyapi.Entities;

namespace sharifyapi.Infrastructure
{
    public class SharifyDbContext : IdentityDbContext<User, Role, Guid>
    {
        public SharifyDbContext(DbContextOptions<SharifyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Playlist> Playlists { get; set; }

        public virtual DbSet<Song> Songs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            builder.Entity<Role>().ToTable("Roles");

            builder.Entity<User>().ToTable("Users");

            builder.Entity<User>()
                .HasMany(e => e.Playlists)
                .WithOne(e => e.CreatedBy);

            builder.Entity<User>()
                .HasIndex(e => e.Name)
                .IsUnique();

            builder.Entity<User>()
                .HasMany(e => e.LikedSongs)
                .WithMany(e => e.LikedBy);

            builder.Entity<Song>()
                .Property(e => e.ListenCount)
                .HasDefaultValue(0);

            builder.Entity<Song>()
                .HasOne(e => e.Author)
                .WithMany(e => e.AddedSongs);

            builder.Entity<Song>()
                .Property(e => e.CreationDate)
                .HasDefaultValueSql("getdate()");

            builder.Entity<Song>()
                .Property(e => e.Status)
                .HasDefaultValue(Status.Pending);

            builder.Entity<Playlist>()
                .HasMany(e => e.Songs)
                .WithMany(e => e.Playlists);

            builder.Entity<Playlist>()
                .HasMany(e => e.LikedBy)
                .WithMany(e => e.LikedPlaylists);

            builder.Entity<Playlist>()
                .Property(e => e.CreationDate)
                .HasDefaultValueSql("getdate()");
        }
    }
}
