﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using sharifyapi.DTO.Request;
using sharifyapi.Entities;
using sharifyapi.Interfaces;

namespace sharifyapi.Infrastructure
{
    public class SharifyDbContextSeed
    {
        private const string SONGS_DIRECTORY = @"D:\Uni\4 Kurs\Saitynai\songs";

        public static async Task SeedAuthAsync(UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            await roleManager.CreateAsync(new Role(Constants.Roles.ADMIN));
            await roleManager.CreateAsync(new Role(Constants.Roles.USER));

            var adminEmail = "karolisvaitele@gmail.com";
            var adminUserPassword = "Testing1!";
            var adminUsername = "Admin";

            var adminUser = new User
            {
                UserName = adminEmail,
                Email = adminEmail,
                Name = adminUsername,
            };

            await userManager.CreateAsync(adminUser, adminUserPassword);
            adminUser = await userManager.FindByNameAsync(adminEmail);
            await userManager.AddToRoleAsync(adminUser, Constants.Roles.ADMIN);
        }

        public static async Task SeedDataAsync(ISongService songService, List<User> users)
        {
            var random = new Random();
            foreach (var file in Directory.EnumerateFiles(SONGS_DIRECTORY))
            {
                var bytes = File.ReadAllBytes(file);
                var fileBase64 = Convert.ToBase64String(bytes);

                var genres = Enum.GetValues(typeof(Genre));

                var createSongRequest = new CreateSongRequest()
                {
                    FileName = file,
                    FileType = "audio/mpeg",
                    Genre = ((Genre)genres.GetValue(random.Next(genres.Length))).ToString(),
                    SongBase64 = fileBase64,
                    Title = Path.GetFileNameWithoutExtension(file),
                };
                var user = users[random.Next(users.Count)];
                var userId = user.Id.ToString();

                await songService.CreateSong(createSongRequest, userId);
            }
        }

        public static async Task<List<User>> SeedUsersAsync(UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            var users = new List<User>();
            for (var i = 1; i < 6; i++)
            {
                var email = $"karolisvaitele@user{i}.com";
                var userPassword = "Testing1!";
                var username = $"User {i}";

                var user = new User
                {
                    UserName = email,
                    Email = email,
                    Name = username,
                };

                await userManager.CreateAsync(user, userPassword);
                user = await userManager.FindByEmailAsync(email);
                await userManager.AddToRoleAsync(user, Constants.Roles.USER);

                users.Add(user);
            }

            return users;
        }
    }
}
