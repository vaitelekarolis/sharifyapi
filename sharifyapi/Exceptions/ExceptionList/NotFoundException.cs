﻿using System;

namespace sharifyapi.Exceptions.ExceptionList
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message)
            : base(message)
        {
        }
    }
}
