﻿using System;

namespace sharifyapi.Exceptions.ExceptionList
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string message)
            : base(message)
        {
        }
    }
}
