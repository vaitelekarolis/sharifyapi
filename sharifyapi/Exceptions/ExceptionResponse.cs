﻿using Newtonsoft.Json;

namespace sharifyapi.Exceptions
{
    public class ExceptionResponse
    {
        public ExceptionResponse(int statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }

        public int StatusCode { get; set; }

        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
