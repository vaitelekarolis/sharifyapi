﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace sharifyapi.Exceptions
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ExceptionHandler _exceptionHandler;

        public ExceptionFilter()
        {
            _exceptionHandler = new ExceptionHandler();
        }

        public override async Task OnExceptionAsync(ExceptionContext context)
        {
            await HandleExceptionAsync(context);
        }

        private Task HandleExceptionAsync(ExceptionContext context)
        {
            var exceptionResponse = _exceptionHandler.Handle(context.Exception);
            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.StatusCode = exceptionResponse.StatusCode;

            return context.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(exceptionResponse));
        }
    }
}
