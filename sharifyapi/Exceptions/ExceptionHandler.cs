﻿using System;
using System.Collections.Generic;
using System.Net;
using sharifyapi.Exceptions.ExceptionList;

namespace sharifyapi.Exceptions
{
    public class ExceptionHandler
    {
        private Dictionary<Type, Func<Exception, ExceptionResponse>> _exceptionHandlers = new Dictionary<Type, Func<Exception, ExceptionResponse>>
        {
            { typeof(NotFoundException),  HandleNotFoundException },
            { typeof(BadRequestException), HandleBadRequestException },
            { typeof(AlreadyExistsException), HandleAlreadyExistsException },
        };

        public ExceptionResponse Handle(Exception exception)
        {
            Type exceptionType = exception.GetType();
            if (_exceptionHandlers.ContainsKey(exceptionType))
            {
                return _exceptionHandlers[exceptionType].Invoke(exception);
            }

            return HandleUnknownException(exception);
        }

        private static ExceptionResponse HandleNotFoundException(Exception exception)
        {
            return new ExceptionResponse((int)HttpStatusCode.NotFound, exception.Message);
        }

        private static ExceptionResponse HandleBadRequestException(Exception exception)
        {
            return new ExceptionResponse((int)HttpStatusCode.BadRequest, exception.Message);
        }

        private static ExceptionResponse HandleAlreadyExistsException(Exception exception)
        {
            return new ExceptionResponse((int)HttpStatusCode.Conflict, exception.Message);
        }

        private static ExceptionResponse HandleUnknownException(Exception exception)
        {
            return new ExceptionResponse((int)HttpStatusCode.InternalServerError, "Unhandled server error occurred");
        }
    }
}
