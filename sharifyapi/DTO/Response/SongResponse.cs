﻿using System;

namespace sharifyapi.DTO.Response
{
    public class SongResponse
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Genre { get; set; }

        public long ListenCount { get; set; }

        public bool IsLiked { get; set; }

        public string Picture { get; set; }
    }
}
