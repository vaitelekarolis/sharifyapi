﻿using System;
using System.Collections.Generic;

namespace sharifyapi.DTO.Response
{
    public class PlaylistGeneralResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int LikeCount { get; set; }

        public bool IsLiked { get; set; }

        public bool IsShared { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedByPicture { get; set; }

        public Guid? FirstSongId { get; set; }

        public bool IsMine { get; set; }

        public List<string> Genres { get; set; }
    }
}
