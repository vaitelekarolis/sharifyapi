﻿namespace sharifyapi.DTO.Response
{
    public class AuthenticateResponse
    {
        public string Token { get; set; }
    }
}
