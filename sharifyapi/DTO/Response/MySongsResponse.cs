﻿using System.Collections.Generic;

namespace sharifyapi.DTO.Response
{
    public class MySongsResponse
    {
        public IList<MySongResponse> Songs { get; set; }
    }
}
