﻿using System.Collections.Generic;

namespace sharifyapi.DTO.Response
{
    public class PlaylistsGeneralResponse
    {
        public IEnumerable<PlaylistGeneralResponse> Playlists { get; set; }
    }
}
