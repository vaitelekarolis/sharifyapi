﻿namespace sharifyapi.DTO.Response
{
    public class UserResponse
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public string Role { get; set; }

        public string Picture { get; set; }
    }
}
