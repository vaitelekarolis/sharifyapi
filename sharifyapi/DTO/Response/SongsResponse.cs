﻿using System.Collections.Generic;

namespace sharifyapi.DTO.Response
{
    public class SongsResponse
    {
        public IEnumerable<SongResponse> Songs { get; set; }
    }
}
