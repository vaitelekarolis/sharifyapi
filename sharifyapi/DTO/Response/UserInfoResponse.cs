﻿using System.Collections.Generic;

namespace sharifyapi.DTO.Response
{
    public class UserInfoResponse
    {
        public string Name { get; set; }

        public string Picture { get; set; }

        public string Description { get; set; }

        public List<string> Genres { get; set; }
    }
}
