﻿using System;

namespace sharifyapi.DTO.Response
{
    public class PlaySongResponse : SongResponse
    {
        public string SongName { get; set; }

        public Guid? NextSongId { get; set; }

        public Guid? PreviousSongId { get; set; }
    }
}
