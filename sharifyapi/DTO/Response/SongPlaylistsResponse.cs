﻿using System;

namespace sharifyapi.DTO.Response
{
    public class SongPlaylistsResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool HasSong { get; set; }
    }
}
