﻿using System.Collections.Generic;

namespace sharifyapi.DTO.Response
{
    public class UsersResponse
    {
        public IEnumerable<UserResponse> Users { get; set; }
    }
}
