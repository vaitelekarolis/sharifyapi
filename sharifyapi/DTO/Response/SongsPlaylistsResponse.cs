﻿using System.Collections.Generic;

namespace sharifyapi.DTO.Response
{
    public class SongsPlaylistsResponse
    {
        public IList<SongPlaylistsResponse> Playlists { get; set; }
    }
}
