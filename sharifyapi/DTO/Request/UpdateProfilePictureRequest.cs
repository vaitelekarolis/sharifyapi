﻿namespace sharifyapi.DTO.Request
{
    public class UpdateProfilePictureRequest
    {
        public string ProfilePicture { get; set; }
    }
}
