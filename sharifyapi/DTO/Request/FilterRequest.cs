﻿using System;
using System.Collections.Generic;
using System.Linq;
using sharifyapi.Entities;
using sharifyapi.Exceptions.ExceptionList;

namespace sharifyapi.DTO.Request
{
    public class FilterRequest : Pagination
    {
        public string Search { get; set; }

        public string Genre { get; set; }

        public string Status { get; set; }

        public List<Genre> GetGenresList()
        {
            var genres = new List<Genre>();
            if (string.IsNullOrEmpty(Genre))
            {
                return genres;
            }

            try
            {
                genres = Genre.Split(',').Select(x => (Genre)Enum.Parse(typeof(Genre), x)).ToList();
            }
            catch
            {
                throw new BadRequestException("Invalid genre");
            }

            return genres;
        }
    }
}
