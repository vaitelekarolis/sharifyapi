﻿using System.ComponentModel.DataAnnotations;

namespace sharifyapi.DTO.Request
{
    public class CreateSongRequest
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Genre { get; set; }

        [Required]
        public string SongBase64 { get; set; }

        [Required]
        public string FileType { get; set; }

        [Required]
        public string FileName { get; set; }
    }
}
