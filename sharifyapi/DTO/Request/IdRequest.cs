﻿using System;

namespace sharifyapi.DTO.Request
{
    public class IdRequest
    {
        public Guid Id { get; set; }
    }
}
