﻿using System;
using System.Collections.Generic;

namespace sharifyapi.DTO.Request
{
    public class PlaySongRequest
    {
        public Guid SongId { get; set; }

        public bool Shuffle { get; set; }

        public Guid? PlaylistId { get; set; }

        public List<Guid> PlayedSongs { get; set; }
    }
}
