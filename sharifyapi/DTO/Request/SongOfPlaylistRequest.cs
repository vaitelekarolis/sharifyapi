﻿using System;

namespace sharifyapi.DTO.Request
{
    public class SongOfPlaylistRequest
    {
        public Guid PlaylistId { get; set; }

        public Guid SongId { get; set; }
    }
}
