﻿namespace sharifyapi.DTO.Request
{
    public class UpdateUserDescriptionRequest
    {
        public string Description { get; set; }
    }
}
