﻿using System;

namespace sharifyapi.DTO.Request
{
    public class UpdatePlaylistRequest
    {
        public Guid Id { get; set; }

        public string Description { get; set; }
    }
}
