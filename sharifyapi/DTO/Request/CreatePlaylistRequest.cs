﻿using System.ComponentModel.DataAnnotations;

namespace sharifyapi.DTO.Request
{
    public class CreatePlaylistRequest
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
