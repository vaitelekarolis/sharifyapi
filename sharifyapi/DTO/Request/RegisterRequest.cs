﻿using System.ComponentModel.DataAnnotations;

namespace sharifyapi.DTO.Request
{
    public class RegisterRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
