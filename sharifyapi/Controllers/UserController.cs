﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Interfaces;

namespace sharifyapi.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> Authenticate(AuthenticateRequest request)
        {
            var result = await _userService.Authenticate(request);
            Response.Cookies.Append(Constants.JWT_TOKEN_COOKIE_NAME, result.Token, new CookieOptions() { HttpOnly = true, Secure = true, MaxAge = new TimeSpan(7, 0, 0, 0), SameSite = SameSiteMode.None });
            return NoContent();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Logout()
        {
            await _userService.Logout();
            Response.Cookies.Append(Constants.JWT_TOKEN_COOKIE_NAME, string.Empty, new CookieOptions() { HttpOnly = true, Secure = true, Expires = DateTime.Now.AddDays(-10), SameSite = SameSiteMode.None });
            return NoContent();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<UserResponse>> Register(RegisterRequest request)
        {
            var user = await _userService.Register(request);
            return Ok(user);
        }

        [HttpGet]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<UserResponse>> Get()
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            var user = await _userService.GetUser(currentUserId);
            return Ok(user);
        }

        [HttpGet("{username}")]
        public async Task<ActionResult<UserInfoResponse>> GetByName(string username)
        {
            var user = await _userService.GetUserByName(username);
            return Ok(user);
        }

        [HttpGet("All")]
        [Authorize(Roles = Constants.Roles.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<UsersResponse>> GetAll()
        {
            var users = await _userService.GetAll();
            return Ok(users);
        }

        [HttpDelete]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteUser()
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _userService.Delete(currentUserId);
            return NoContent();
        }

        [HttpPut("ProfilePicture")]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> UpdateProfilePicture(UpdateProfilePictureRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _userService.UpdateProfilePicture(currentUserId, request.ProfilePicture);

            return NoContent();
        }

        [HttpPut("Description")]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> UpdateDescription(UpdateUserDescriptionRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _userService.UpdateDescription(currentUserId, request.Description);

            return NoContent();
        }
    }
}
