﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Interfaces;

namespace sharifyapi.Controllers
{
    [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PlaylistController : BaseController
    {
        private readonly IPlaylistService _playlistService;

        public PlaylistController(IPlaylistService playlistService)
        {
            _playlistService = playlistService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PlaylistGeneralResponse>> GetById(Guid id)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            var playlist = await _playlistService.GetById(id, currentUserId);
            return Ok(playlist);
        }

        [HttpGet("{id}/Songs")]
        public async Task<ActionResult<SongsResponse>> GetSongs(Guid id, [FromQuery] FilterRequest filter)
        {
            var playlist = await _playlistService.GetSongs(id, filter);
            return Ok(playlist);
        }

        [HttpGet("My")]
        public async Task<ActionResult<PlaylistsGeneralResponse>> GetMyPlaylists([FromQuery] FilterRequest filter)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            var playlists = await _playlistService.GetMyPlaylists(currentUserId, filter);
            return Ok(playlists);
        }

        [HttpGet("Liked")]
        public async Task<ActionResult<PlaylistsGeneralResponse>> GetLikedPlaylists([FromQuery] FilterRequest filter)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            var playlists = await _playlistService.GetLikedPlaylists(currentUserId, filter);
            return Ok(playlists);
        }

        [HttpGet("SongsPlaylists/{songId}")]
        public async Task<ActionResult<SongsPlaylistsResponse>> GetSongsPlaylists(Guid songId)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            var playlists = await _playlistService.GetSongsPlaylists(songId, currentUserId);
            return Ok(playlists);
        }

        [HttpGet("Shared")]
        public async Task<ActionResult<PlaylistsGeneralResponse>> GetAllShared([FromQuery] FilterRequest filter)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            var playlists = await _playlistService.GetAllShared(currentUserId, filter);
            return Ok(playlists);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreatePlaylistRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _playlistService.CreatePlaylist(request, currentUserId);
            return NoContent();
        }

        [HttpPost("Song")]
        public async Task<IActionResult> AddSong(SongOfPlaylistRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _playlistService.AddSong(request, currentUserId);
            return NoContent();
        }

        [HttpDelete("Song")]
        public async Task<IActionResult> RemoveSong(SongOfPlaylistRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _playlistService.RemoveSong(request, currentUserId);
            return NoContent();
        }

        [HttpPut]
        public async Task<IActionResult> Update(UpdatePlaylistRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _playlistService.UpdatePlaylist(request, currentUserId);
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(IdRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _playlistService.DeletePlaylist(request, currentUserId);
            return NoContent();
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ToggleShare(IdRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _playlistService.ToggleSharePaylist(request, currentUserId);
            return NoContent();
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ToggleLike(IdRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _playlistService.ToggleLikePlaylist(request, currentUserId);
            return NoContent();
        }
    }
}
