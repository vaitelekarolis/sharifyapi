﻿using Microsoft.AspNetCore.Mvc;

namespace sharifyapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
    }
}
