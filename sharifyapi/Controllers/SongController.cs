﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Interfaces;

namespace sharifyapi.Controllers
{
    [Authorize]
    public class SongController : BaseController
    {
        private readonly ISongService _songService;

        public SongController(ISongService songService)
        {
            _songService = songService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<SongsResponse>> GetAll([FromQuery] FilterRequest filter)
        {
            string currentUserId = null;
            if (User.Identity.IsAuthenticated)
            {
                currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            }

            var songs = await _songService.GetAll(currentUserId, filter);
            return Ok(songs);
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<SongResponse>> GetById(Guid id)
        {
            string currentUserId = null;
            if (User.Identity.IsAuthenticated)
            {
                currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            }

            var song = await _songService.GetById(id, currentUserId);
            return Ok(song);
        }

        [HttpGet("Liked")]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<SongsResponse>> GetLiked([FromQuery] FilterRequest filter)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;

            var songs = await _songService.GetLiked(currentUserId, filter);
            return Ok(songs);
        }

        [HttpGet("CreatedBy/{userName}")]
        [AllowAnonymous]
        public async Task<ActionResult<SongsResponse>> GetCreatedBy(string userName, [FromQuery] FilterRequest filterRequest, int? takeTop)
        {
            var songs = await _songService.GetCreatedBy(userName, filterRequest, takeTop);
            return Ok(songs);
        }

        [HttpGet("My")]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<SongsResponse>> GetMy([FromQuery] FilterRequest filterRequest)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            var songs = await _songService.GetMy(currentUserId, filterRequest);
            return Ok(songs);
        }

        [HttpGet("Requests")]
        [Authorize(Roles = Constants.Roles.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<SongsResponse>> GetSongRequests([FromQuery] FilterRequest filterRequest)
        {
            var songs = await _songService.GetSongRequests(filterRequest);
            return Ok(songs);
        }

        [HttpPut("Play")]
        [AllowAnonymous]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<PlaySongResponse>> GetPlaySong(PlaySongRequest request)
        {
            string currentUserId = null;
            if (User.Identity.IsAuthenticated)
            {
                currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            }

            var song = await _songService.GetPlaySong(currentUserId, request);
            return Ok(song);
        }

        [HttpGet("Genres")]
        [AllowAnonymous]
        public ActionResult<List<string>> GetSongGenres()
        {
            var songGenres = _songService.GetSongGenres();
            return Ok(songGenres);
        }

        [HttpGet("Statuses")]
        [AllowAnonymous]
        public ActionResult<List<string>> GetSongStatuses()
        {
            var songStatuses = _songService.GetSongStatuses();
            return Ok(songStatuses);
        }

        [HttpGet("TopPlayed")]
        [AllowAnonymous]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<SongsResponse>> GetTopPlayed()
        {
            string currentUserId = null;
            if (User.Identity.IsAuthenticated)
            {
                currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            }

            var songs = await _songService.GetTopPlayed(currentUserId);
            return Ok(songs);
        }

        [HttpGet("Newest")]
        [AllowAnonymous]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<SongsResponse>> GetNewest()
        {
            string currentUserId = null;
            if (User.Identity.IsAuthenticated)
            {
                currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            }

            var songs = await _songService.GetNewest(currentUserId);
            return Ok(songs);
        }

        [HttpPost]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> CreateSongRequest(CreateSongRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _songService.CreateSong(request, currentUserId);
            return NoContent();
        }

        [HttpPut("[action]")]
        [Authorize(Roles = Constants.Roles.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Confirm(IdRequest request)
        {
            await _songService.ConfirmSong(request);
            return NoContent();
        }

        [HttpPut("[action]")]
        [Authorize(Roles = Constants.Roles.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Reject(IdRequest request)
        {
            await _songService.RejectSong(request);
            return NoContent();
        }

        [HttpPut("[action]")]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ToggleLike(IdRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _songService.ToggleLikeSong(request, currentUserId);
            return NoContent();
        }

        [HttpDelete]
        [Authorize(Roles = Constants.Roles.ADMIN + "," + Constants.Roles.USER, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteSong(IdRequest request)
        {
            var currentUserId = User.FindFirst(ClaimTypes.Sid).Value;
            await _songService.DeleteSong(currentUserId, request.Id);
            return NoContent();
        }
    }
}
