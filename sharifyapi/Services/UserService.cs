﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Entities;
using sharifyapi.Exceptions.ExceptionList;
using sharifyapi.Interfaces;
using sharifyapi.Resources;

namespace sharifyapi.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ITokenClaimsService _tokenClaimsService;
        private readonly IMapper _mapper;
        private readonly IRepository _repository;
        private readonly ISongService _songService;

        public UserService(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ITokenClaimsService tokenClaimsService,
            IMapper mapper,
            IRepository repository,
            ISongService songService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenClaimsService = tokenClaimsService;
            _mapper = mapper;
            _repository = repository;
            _songService = songService;
        }

        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest request)
        {
            var result = await _signInManager.PasswordSignInAsync(request.Email, request.Password, false, true);
            if (!result.Succeeded)
            {
                throw new NotFoundException(Errors.LoginFailed);
            }

            var token = await _tokenClaimsService.GetTokenAsync(request.Email);

            return new AuthenticateResponse() { Token = token };
        }

        public async Task<UserResponse> Register(RegisterRequest request)
        {
            var userExists = await _userManager.Users.AnyAsync(x => x.Name == request.Username);
            if (userExists)
            {
                throw new BadRequestException("User with this username already exists");
            }

            var user = _mapper.Map<User>(request);

            user.UserName = request.Email;
            user.Name = request.Username;

            var result = await _userManager.CreateAsync(user, request.Password);
            if (!result.Succeeded)
            {
                throw new BadRequestException(result.Errors.First().Description);
            }

            await _userManager.AddToRoleAsync(user, Constants.Roles.USER);

            var response = _mapper.Map<UserResponse>(user);
            response.Role = Constants.Roles.USER;

            return response;
        }

        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<UserResponse> GetUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            var userResponse = _mapper.Map<UserResponse>(user);
            userResponse.Role = await GetUserRole(user);

            return userResponse;
        }

        public async Task<UserInfoResponse> GetUserByName(string username)
        {
            var user = await _userManager.Users.Where(x => x.Name == username).FirstOrDefaultAsync();
            if (user == null)
            {
                throw new NotFoundException("User does not exist");
            }

            var userResponse = _mapper.Map<UserInfoResponse>(user);
            userResponse.Genres = GetGenreOfUser(user);

            return userResponse;
        }

        public async Task<UsersResponse> GetAll()
        {
            var users = await _userManager.Users.ToListAsync();

            var usersResponse = new List<UserResponse>();
            foreach (var user in users)
            {
                var userResponse = _mapper.Map<UserResponse>(user);
                userResponse.Role = await GetUserRole(user);

                usersResponse.Add(userResponse);
            }

            return new UsersResponse() { Users = usersResponse };
        }

        public async Task Delete(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                throw new NotFoundException(Errors.UserDoesNotExist);
            }

            await DeleteUserInfoAsync(user);
            await _userManager.DeleteAsync(user);
        }

        public async Task UpdateProfilePicture(string userId, string profilePicture)
        {
            if (profilePicture.Length > 80000)
            {
                throw new BadRequestException("Chosen image is too big");
            }

            var user = await _userManager.FindByIdAsync(userId);

            user.Picture = profilePicture;

            await _userManager.UpdateAsync(user);
        }

        public async Task UpdateDescription(string userId, string description)
        {
            var user = await _userManager.FindByIdAsync(userId);

            user.Description = description.Trim();

            await _userManager.UpdateAsync(user);
        }

        public List<string> GetGenreOfUser(User user)
        {
            var usersSongs = user.AddedSongs
                .Where(x => x.Status == Status.Confirmed);
            if (!usersSongs.Any())
            {
                return null;
            }

            var genresGrouped = usersSongs
                .GroupBy(x => x.Genre)
                .Select(x => new { genre = x.Key, count = x.Count() })
                .OrderByDescending(x => x.count)
                .ToDictionary(x => x.genre, x => x.count);

            var maxCount = genresGrouped.First().Value;
            var maxGenres = genresGrouped.Where(x => x.Value == maxCount)
                .Select(x => x.Key.ToString());

            return maxGenres.ToList();
        }

        private async Task<string> GetUserRole(User user)
        {
            return (await _userManager.GetRolesAsync(user)).FirstOrDefault();
        }

        private async Task DeleteUserInfoAsync(User user)
        {
            var playlists = user.Playlists;
            await _repository.DeleteRangeAsync(playlists);

            await _songService.DeleteSongs(user);
        }
    }
}
