﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Entities;
using sharifyapi.Exceptions.ExceptionList;
using sharifyapi.Filters.Songs;
using sharifyapi.Interfaces;
using sharifyapi.Resources;

namespace sharifyapi.Services
{
    public class SongService : ISongService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IStorageService _storageService;

        public SongService(IRepository repository, IMapper mapper, UserManager<User> userManager, IStorageService storageService)
        {
            _repository = repository;
            _mapper = mapper;
            _userManager = userManager;
            _storageService = storageService;
        }

        public async Task<SongsResponse> GetAll(string userId, FilterRequest filterRequest)
        {
            var allSongs = _repository.ListQuery<Song>()
                .Where(song => song.Status == Status.Confirmed)
                .OrderByDescending(song => song.ListenCount);

            var filter = new SongSearchFilter();
            filter.SetNext(new SongGenreFilter())
                  .SetNext(new SongPageFilter());

            var filteredSongs = filter.Filter(allSongs, filterRequest);

            var songs = await filteredSongs.ToListAsync();

            var user = await _userManager.FindByIdAsync(userId);

            var songList = new List<SongResponse>();

            foreach (Song song in songs)
            {
                var songResponse = _mapper.Map<SongResponse>(song);
                songResponse.IsLiked = song.LikedBy.Contains(user);
                songList.Add(songResponse);
            }

            return new SongsResponse() { Songs = songList };
        }

        public async Task<SongsResponse> GetTopPlayed(string userId)
        {
            var songs = await _repository.ListQuery<Song>()
                .Where(x => x.Status == Status.Confirmed)
                .OrderByDescending(x => x.ListenCount)
                .Take(10)
                .ToListAsync();

            var user = await _userManager.FindByIdAsync(userId);

            var songList = new List<SongResponse>();

            foreach (Song song in songs)
            {
                var songResponse = _mapper.Map<SongResponse>(song);
                songResponse.IsLiked = song.LikedBy.Contains(user);
                songList.Add(songResponse);
            }

            return new SongsResponse() { Songs = songList };
        }

        public async Task<SongsResponse> GetNewest(string userId)
        {
            var songs = await _repository.ListQuery<Song>()
                .Where(x => x.Status == Status.Confirmed)
                .OrderByDescending(x => x.CreationDate)
                .Take(10)
                .ToListAsync();

            var user = await _userManager.FindByIdAsync(userId);

            var songList = new List<SongResponse>();

            foreach (Song song in songs)
            {
                var songResponse = _mapper.Map<SongResponse>(song);
                songResponse.IsLiked = song.LikedBy.Contains(user);
                songList.Add(songResponse);
            }

            return new SongsResponse() { Songs = songList };
        }

        public async Task<SongResponse> GetById(Guid id, string userId)
        {
            var song = await _repository.GetByIdAsync<Song>(id);
            var user = await _userManager.FindByIdAsync(userId);

            if (song == null || song.Status != Status.Confirmed)
            {
                throw new NotFoundException(Errors.NotFound);
            }

            var songResponse = _mapper.Map<SongResponse>(song);
            songResponse.IsLiked = song.LikedBy.Contains(user);

            return songResponse;
        }

        public async Task<SongsResponse> GetLiked(string userId, FilterRequest filterRequest)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var songsQuery = _repository.ListQuery<Song>()
                .Where(x => x.LikedBy.Contains(user));

            var filter = new SongSearchFilter();
            filter.SetNext(new SongGenreFilter())
                  .SetNext(new SongPageFilter());

            var songs = await filter.Filter(songsQuery, filterRequest)
                .ToListAsync();

            var songList = new List<SongResponse>();

            foreach (Song song in songs)
            {
                var songResponse = _mapper.Map<SongResponse>(song);
                songResponse.IsLiked = song.LikedBy.Contains(user);
                songList.Add(songResponse);
            }

            return new SongsResponse() { Songs = songList };
        }

        public async Task<SongsResponse> GetCreatedBy(string userName, FilterRequest filterRequest, int? takeTop)
        {
            var user = await _userManager.Users.Where(x => x.Name == userName).FirstOrDefaultAsync();
            if (user == null)
            {
                throw new BadRequestException("This user does not exist");
            }

            var songsQuery = _repository.ListQuery<Song>().Where(x => x.Author == user && x.Status == Status.Confirmed);
            if (takeTop.HasValue)
            {
                songsQuery = songsQuery.OrderByDescending(x => x.ListenCount)
                    .Take(takeTop.Value);
            }

            var filter = new SongSearchFilter();
            filter.SetNext(new SongGenreFilter())
                  .SetNext(new SongPageFilter());

            var songs = await filter.Filter(songsQuery, filterRequest)
                .ToListAsync();

            var songList = new List<SongResponse>();

            foreach (Song song in songs)
            {
                var songResponse = _mapper.Map<SongResponse>(song);
                songResponse.IsLiked = song.LikedBy.Contains(user);
                songList.Add(songResponse);
            }

            return new SongsResponse() { Songs = songList };
        }

        public async Task<MySongsResponse> GetSongRequests(FilterRequest filterRequest)
        {
            var songsQuery = _repository.ListQuery<Song>()
                .OrderByDescending(x => x.Status == Status.Pending)
                .ThenByDescending(x => x.Status == Status.Confirmed)
                .ThenByDescending(x => x.Status == Status.Rejected)
                .ThenByDescending(x => x.CreationDate);

            var filter = new SongSearchFilter();
            filter.SetNext(new SongGenreFilter())
                  .SetNext(new SongStatusFilter())
                  .SetNext(new SongPageFilter());

            var songs = await filter.Filter(songsQuery, filterRequest)
                .ToListAsync();

            var songList = new List<MySongResponse>();

            foreach (Song song in songs)
            {
                var songResponse = _mapper.Map<MySongResponse>(song);
                songList.Add(songResponse);
            }

            return new MySongsResponse() { Songs = songList };
        }

        public async Task<PlaySongResponse> GetPlaySong(string userId, PlaySongRequest request)
        {
            var song = await _repository.GetByIdAsync<Song>(request.SongId);
            var user = await _userManager.FindByIdAsync(userId);

            if (song == null)
            {
                throw new NotFoundException(Errors.NotFound);
            }

            var songResponse = _mapper.Map<PlaySongResponse>(song);
            songResponse.IsLiked = song.LikedBy.Contains(user);

            if (request.PlaylistId.HasValue && user != null)
            {
                var songIds = await GetPreviousAndNextSongId(request);
                songResponse.PreviousSongId = songIds.Item1;
                songResponse.NextSongId = songIds.Item2;
            }

            if (song.Status == Status.Confirmed)
            {
                song.ListenCount++;
                await _repository.UpdateAsync(song);
            }

            return songResponse;
        }

        public List<string> GetSongGenres()
        {
            var songGenres = new List<string>();

            foreach (Genre genre in Enum.GetValues(typeof(Genre)))
            {
                songGenres.Add(genre.ToString());
            }

            return songGenres;
        }

        public List<string> GetSongStatuses()
        {
            var songStatuses = new List<string>();

            foreach (Status status in Enum.GetValues(typeof(Status)))
            {
                songStatuses.Add(status.ToString());
            }

            return songStatuses;
        }

        public async Task CreateSong(CreateSongRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var newSong = _mapper.Map<Song>(request);
            newSong.Author = user;

            var songExtension = GetSongExtension(request.FileName);
            var songName = $"{newSong.Author.Name} - {newSong.Title}{songExtension}";
            var doesSongExist = await _repository.ListQuery<Song>().AnyAsync(x => x.SongName == songName);
            if (doesSongExist)
            {
                throw new BadRequestException("You have already created song with this tite");
            }

            newSong.SongName = songName;

            await _repository.AddAsync(newSong);

            await _storageService.UploadSongAsync(request.SongBase64, request.FileType, songName);
        }

        public async Task ConfirmSong(IdRequest request)
        {
            var song = await _repository.GetByIdAsync<Song>(request.Id);

            if (song == null)
            {
                throw new NotFoundException(Errors.SongRequestFailed);
            }

            song.Status = Status.Confirmed;

            await _repository.UpdateAsync(song);
        }

        public async Task RejectSong(IdRequest request)
        {
            var song = await _repository.GetByIdAsync<Song>(request.Id);

            if (song == null)
            {
                throw new NotFoundException(Errors.SongRequestFailed);
            }

            song.Status = Status.Rejected;

            await _repository.UpdateAsync(song);
        }

        public async Task DeleteSong(string userId, Guid songId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var song = await _repository.GetByIdAsync<Song>(songId);

            if (song == null)
            {
                throw new NotFoundException(Errors.SongRequestFailed);
            }

            if (song.Author != user)
            {
                throw new BadRequestException("The song does not belong to you");
            }

            await _storageService.DeleteSongAsync(song.SongName);

            await _repository.DeleteAsync(song);
        }

        public async Task ToggleLikeSong(IdRequest request, string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var song = await _repository.GetByIdAsync<Song>(request.Id);

            if (user == null || song == null)
            {
                throw new NotFoundException(Errors.PlaylistUpdateFailed);
            }

            if (song.LikedBy.Contains(user))
            {
                song.LikedBy.Remove(user);
            }
            else
            {
                song.LikedBy.Add(user);
            }

            await _repository.UpdateAsync(song);
        }

        public async Task<MySongsResponse> GetMy(string userId, FilterRequest filterRequest)
        {
            var songsQuery = _repository.ListQuery<Song>()
                .Where(x => x.AuthorId.ToString() == userId);

            var filter = new SongSearchFilter();
            filter.SetNext(new SongGenreFilter())
                  .SetNext(new SongStatusFilter())
                  .SetNext(new SongPageFilter());

            var songs = await filter.Filter(songsQuery, filterRequest)
                .ToListAsync();

            var songList = new List<MySongResponse>();

            foreach (Song song in songs)
            {
                var songResponse = _mapper.Map<MySongResponse>(song);
                songResponse.IsLiked = song.LikedBy.Any(x => x.Id.ToString() == userId);
                songList.Add(songResponse);
            }

            return new MySongsResponse() { Songs = songList };
        }

        public async Task DeleteSongs(User user)
        {
            var userSongFiles = user.AddedSongs.Select(x => x.SongName);
            foreach (var userSongFile in userSongFiles)
            {
                await _storageService.DeleteSongAsync(userSongFile);
            }

            await _repository.DeleteRangeAsync(user.AddedSongs);
        }

        private static string GetSongExtension(string fileName)
        {
            var fileExtension = Path.GetExtension(fileName);
            var songExtension = string.IsNullOrEmpty(fileExtension) ? ".mp3" : fileExtension;
            return songExtension;
        }

        private async Task<(Guid?, Guid?)> GetPreviousAndNextSongId(PlaySongRequest request)
        {
            var playlist = await _repository.GetByIdAsync<Playlist>(request.PlaylistId.Value);
            if (playlist == null)
            {
                throw new BadRequestException("Playlist does not exist");
            }

            if (request.Shuffle)
            {
                var playablePlaylistSongs = playlist.Songs.Where(x => !request.PlayedSongs.Contains(x.Id)).ToList();
                Guid? previousSongId = request.PlayedSongs.Count > 1 ? request.PlayedSongs[^2] : null;
                var rand = new Random();
                Guid? nextSongId = playablePlaylistSongs.Count > 0 ? playablePlaylistSongs[rand.Next(playablePlaylistSongs.Count)].Id : null;
                return (previousSongId, nextSongId);
            }

            var playlistSongs = playlist.Songs;
            var nextSongOrDefault = playlistSongs.SkipWhile(x => request.SongId != x.Id).Skip(1).DefaultIfEmpty(playlistSongs[0]).FirstOrDefault();
            Guid? nextSong = nextSongOrDefault?.Id;
            var previousSongOrDefault = playlistSongs.TakeWhile(x => request.SongId != x.Id).DefaultIfEmpty(playlistSongs[playlistSongs.Count - 1]).LastOrDefault();
            Guid? previousSong = previousSongOrDefault?.Id;

            return (previousSong, nextSong);
        }
    }
}
