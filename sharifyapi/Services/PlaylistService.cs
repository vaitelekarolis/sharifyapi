﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using sharifyapi.DTO.Request;
using sharifyapi.DTO.Response;
using sharifyapi.Entities;
using sharifyapi.Exceptions.ExceptionList;
using sharifyapi.Filters.Playlists;
using sharifyapi.Filters.Songs;
using sharifyapi.Interfaces;
using sharifyapi.Resources;

namespace sharifyapi.Services
{
    public class PlaylistService : IPlaylistService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public PlaylistService(IRepository repository, IMapper mapper, UserManager<User> userManager)
        {
            _repository = repository;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<PlaylistGeneralResponse> GetById(Guid id, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlist = await _repository.GetByIdAsync<Playlist>(id);

            if (playlist == null || (playlist.CreatedBy != user && playlist.IsShared == false))
            {
                throw new NotFoundException(Errors.NotFound);
            }

            var playlistResponse = _mapper.Map<PlaylistGeneralResponse>(playlist);
            playlistResponse.IsLiked = playlist.LikedBy.Contains(user);
            playlistResponse.IsMine = playlist.CreatedBy == user && user != null;
            playlistResponse.Genres = GetPlaylistGenreDto(playlist);

            return playlistResponse;
        }

        public async Task<PlaylistsGeneralResponse> GetMyPlaylists(string userId, FilterRequest filterRequest)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlistsQuery = _repository.ListQuery<Playlist>().Where(x => x.CreatedBy == user);

            var filter = new PlaylistSearchFilter();
            filter.SetNext(new PlaylistPageFilter());

            var playlists = await filter.Filter(playlistsQuery, filterRequest)
                .ToListAsync();

            playlists = FilterPlaylistGenre(playlists, filterRequest);

            var playlistsResponse = new List<PlaylistGeneralResponse>();

            foreach (var playlist in playlists)
            {
                var playlistResponse = _mapper.Map<PlaylistGeneralResponse>(playlist);
                playlistResponse.IsLiked = playlist.LikedBy.Contains(user);
                playlistResponse.IsMine = playlist.CreatedBy == user && user != null;
                playlistResponse.Genres = GetPlaylistGenreDto(playlist);

                playlistsResponse.Add(playlistResponse);
            }

            return new PlaylistsGeneralResponse() { Playlists = playlistsResponse };
        }

        public async Task<PlaylistsGeneralResponse> GetLikedPlaylists(string userId, FilterRequest filterRequest)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlistsQuery = _repository.ListQuery<Playlist>().Where(x => x.LikedBy.Contains(user));

            var filter = new PlaylistSearchFilter();
            filter.SetNext(new PlaylistPageFilter());

            var playlists = await filter.Filter(playlistsQuery, filterRequest)
                .ToListAsync();

            playlists = FilterPlaylistGenre(playlists, filterRequest);

            var playlistsResponse = new List<PlaylistGeneralResponse>();

            foreach (var playlist in playlists)
            {
                var playlistResponse = _mapper.Map<PlaylistGeneralResponse>(playlist);
                playlistResponse.IsLiked = playlist.LikedBy.Contains(user);
                playlistResponse.IsMine = playlist.CreatedBy == user && user != null;
                playlistResponse.Genres = GetPlaylistGenreDto(playlist);

                playlistsResponse.Add(playlistResponse);
            }

            return new PlaylistsGeneralResponse() { Playlists = playlistsResponse };
        }

        public async Task<PlaylistsGeneralResponse> GetAllShared(string userId, FilterRequest filterRequest)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlistsQuery = _repository.ListQuery<Playlist>().Where(x => x.IsShared && x.Songs.Count > 1);

            var filter = new PlaylistSearchFilter();
            filter.SetNext(new PlaylistPageFilter());

            var playlists = await filter.Filter(playlistsQuery, filterRequest)
                .ToListAsync();

            playlists = FilterPlaylistGenre(playlists, filterRequest);

            var playlistsResponse = new List<PlaylistGeneralResponse>();

            foreach (var playlist in playlists)
            {
                var playlistResponse = _mapper.Map<PlaylistGeneralResponse>(playlist);
                playlistResponse.IsLiked = playlist.LikedBy.Contains(user);
                playlistResponse.IsMine = playlist.CreatedBy == user;
                playlistResponse.Genres = GetPlaylistGenreDto(playlist);

                playlistsResponse.Add(playlistResponse);
            }

            return new PlaylistsGeneralResponse() { Playlists = playlistsResponse };
        }

        public async Task CreatePlaylist(CreatePlaylistRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            var playlist = _mapper.Map<Playlist>(request);
            playlist.CreatedBy = user;

            await _repository.AddAsync(playlist);
        }

        public async Task AddSong(SongOfPlaylistRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var song = await _repository.GetByIdAsync<Song>(request.SongId);
            var playlist = await _repository.GetByIdAsync<Playlist>(request.PlaylistId);

            if (user == null
                || song == null
                || playlist == null
                || song.Status != Status.Confirmed
                || playlist.CreatedBy != user)
            {
                throw new NotFoundException(Errors.SongAddToPlaylistFailed);
            }

            if (playlist.Songs.Contains(song))
            {
                throw new AlreadyExistsException("You have already added this song to this playlist");
            }

            playlist.Songs.Add(song);

            await _repository.UpdateAsync(playlist);
        }

        public async Task RemoveSong(SongOfPlaylistRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var song = await _repository.GetByIdAsync<Song>(request.SongId);
            var playlist = await _repository.GetByIdAsync<Playlist>(request.PlaylistId);

            if (user == null || song == null || playlist == null || playlist.CreatedBy != user)
            {
                throw new NotFoundException(Errors.SongRemoveFromPlaylistFailed);
            }

            playlist.Songs.Remove(song);

            await _repository.UpdateAsync(playlist);
        }

        public async Task UpdatePlaylist(UpdatePlaylistRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlist = await _repository.GetByIdAsync<Playlist>(request.Id);

            if (user == null || playlist == null || playlist.CreatedBy != user)
            {
                throw new NotFoundException(Errors.PlaylistUpdateFailed);
            }

            playlist.Description = request.Description;

            await _repository.UpdateAsync(playlist);
        }

        public async Task DeletePlaylist(IdRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlist = await _repository.GetByIdAsync<Playlist>(request.Id);

            if (user == null || playlist == null || playlist.CreatedBy != user)
            {
                throw new NotFoundException(Errors.PlaylistUpdateFailed);
            }

            await _repository.DeleteAsync(playlist);
        }

        public async Task ToggleSharePaylist(IdRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlist = await _repository.GetByIdAsync<Playlist>(request.Id);

            if (user == null || playlist == null || playlist.CreatedBy != user)
            {
                throw new NotFoundException(Errors.PlaylistUpdateFailed);
            }

            playlist.IsShared = !playlist.IsShared;

            await _repository.UpdateAsync(playlist);
        }

        public async Task ToggleLikePlaylist(IdRequest request, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlist = await _repository.GetByIdAsync<Playlist>(request.Id);

            if (user == null || playlist == null)
            {
                throw new NotFoundException(Errors.PlaylistUpdateFailed);
            }

            if (playlist.LikedBy.Contains(user))
            {
                playlist.LikedBy.Remove(user);
            }
            else
            {
                playlist.LikedBy.Add(user);
            }

            await _repository.UpdateAsync(playlist);
        }

        public async Task<SongsResponse> GetSongs(Guid id, FilterRequest filterRequest)
        {
            var playlist = await _repository.GetByIdAsync<Playlist>(id);

            if (playlist == null)
            {
                throw new NotFoundException(Errors.NotFound);
            }

            var songsQuery = _repository.ListQuery<Song>()
                .Where(x => x.Playlists.Contains(playlist));

            var filter = new SongSearchFilter();
            filter.SetNext(new SongGenreFilter())
                  .SetNext(new SongPageFilter());

            var songs = await filter.Filter(songsQuery, filterRequest)
                .ToListAsync();

            var playlistSongs = new List<SongResponse>();
            foreach (var song in songs)
            {
                var songResponse = _mapper.Map<SongResponse>(song);
                playlistSongs.Add(songResponse);
            }

            return new SongsResponse() { Songs = playlistSongs };
        }

        public async Task<SongsPlaylistsResponse> GetSongsPlaylists(Guid songId, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var playlists = await _repository.ListQuery<Playlist>().Where(x => x.CreatedBy == user).ToListAsync();
            var playlistsResponse = new List<SongPlaylistsResponse>();

            foreach (var playlist in playlists)
            {
                var playlistResponse = _mapper.Map<SongPlaylistsResponse>(playlist);
                playlistResponse.HasSong = playlist.Songs.Any(x => x.Id == songId);

                playlistsResponse.Add(playlistResponse);
            }

            return new SongsPlaylistsResponse() { Playlists = playlistsResponse };
        }

        private List<Playlist> FilterPlaylistGenre(List<Playlist> playlists, FilterRequest filterRequest)
        {
            var genres = filterRequest.GetGenresList();
            if (genres.Count == 0)
            {
                return playlists;
            }

            playlists = playlists.Where(x => GetPlaylistGenre(x).Any(genre => genres.Contains(genre))).ToList();

            return playlists;
        }

        private List<Genre> GetPlaylistGenre(Playlist playlist)
        {
            if (playlist.Songs.Count == 0)
            {
                return new List<Genre>();
            }

            var playlistGenre = playlist.Songs
                .GroupBy(x => x.Genre)
                .GroupBy(group => group.Count())
                .OrderByDescending(g => g.Key)
                .First()
                .Select(g => g.Key)
                .ToList();

            return playlistGenre;
        }

        private List<string> GetPlaylistGenreDto(Playlist playlist)
        {
            var genres = GetPlaylistGenre(playlist);

            return genres.Select(x => x.ToString()).ToList();
        }
    }
}
