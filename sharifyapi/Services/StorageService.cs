﻿using System;
using System.IO;
using System.Threading.Tasks;
using Google.Cloud.Storage.V1;
using sharifyapi.Interfaces;

namespace sharifyapi.Services
{
    public class StorageService : IStorageService
    {
        private const string BUCKET_NAME = "sharifystorage.appspot.com";
        private readonly StorageClient _storage;

        public StorageService()
        {
            _storage = StorageClient.Create();
        }

        public async Task UploadSongAsync(string fileInBase64, string fileType, string fileName)
        {
            var songInBytes = Convert.FromBase64String(fileInBase64);
            using var stream = new MemoryStream(songInBytes);
            await _storage.UploadObjectAsync(BUCKET_NAME, fileName, fileType, stream);
        }

        public async Task DeleteSongAsync(string fileName)
        {
            await _storage.DeleteObjectAsync(BUCKET_NAME, fileName);
        }
    }
}
